import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { FormsModule , ReactiveFormsModule } from '@angular/forms' 
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store' 
import { EffectsModule } from '@ngrx/effects'
import { StoreDevtoolsModule} from '@ngrx/store-devtools'

import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';
import { DestinoViejeComponent } from './destino-vieje/destino-vieje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';

import { DestinoViajeState, reducerDestinosViajes, initializeDestinoViajeState, DestinoViajeEffects } from './models/destino-viajes-state.model';

const routes: Routes = [
  {path: '', redirectTo: 'home',pathMatch: 'full'},
   {path: 'home', component:ListaDestinosComponent},
  //  {path: 'home', component:FormDestinoViajeComponent},
  // {path: 'destino/:id', component:DestinoDetalleComponent},
  {path: 'destino', component:DestinoDetalleComponent},
]


//redux init
export interface AppState{
  destinos: DestinoViajeState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState ={
  destinos: initializeDestinoViajeState()
}; 
// redux fin init


@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    DestinoViejeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule , 
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
    EffectsModule.forRoot([DestinoViajeEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [DestinosApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { } 