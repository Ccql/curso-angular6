 import {Injectable} from '@angular/core';
 import {Action} from '@ngrx/store';
 import { Actions, Effect, ofType} from '@ngrx/effects';
 import {Observable, of} from 'rxjs';
 import {map} from 'rxjs/operators';
 import { DestinoViaje } from './destino-viaje.model';



 //ESTADO
 export interface DestinoViajeState{
    items : DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
 }

 export const initializeDestinoViajeState = function () {
     return {
         items : [],
         loading: false, 
         favorito: null
     }
 }

 //ACCIONES
 export enum DestinoViajeActionTypes{
     NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
     ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
     VOTE_UP = '[Destinos Viajes]  Vote Up',
     VOTE_DOWN = '[Destinos Viajes] Vote Down'
 }

export class NuevoDestinoAction implements Action {
    type = DestinoViajeActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}


export class ElegidoFavoritoAction implements Action {
    type = DestinoViajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinoViajeActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinoViajeActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export type DestinoViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction; 

// REDUCERS 

export function reducerDestinosViajes(
    state: DestinoViajeState,
    action: DestinoViajesActions,
):DestinoViajeState{
    switch (action.type) {
        case DestinoViajeActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinoViajeActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x=> x.setSelected(false));
            let fav:DestinoViaje = ( action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinoViajeActionTypes.VOTE_UP : {
            const d:DestinoViaje = ( action as VoteUpAction).destino;
            d.voteUp(); 
            return {
                ...state
            };
        }
        case DestinoViajeActionTypes.VOTE_DOWN : {
            const d:DestinoViaje = ( action as VoteDownAction).destino;
            d.voteDown(); 
            return {
                ...state
            };
        }
        
    }
    return state;
}

// EFFECTS
@Injectable()
export class DestinoViajeEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajeActionTypes.NUEVO_DESTINO),
        map((action : NuevoDestinoAction) =>new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) {}
}
  