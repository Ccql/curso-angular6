import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../models/destino-viajes-state.model';

@Component({
  selector: 'app-destino-vieje',
  templateUrl: './destino-vieje.component.html',
  styleUrls: ['./destino-vieje.component.css']
})
export class DestinoViejeComponent implements OnInit {
  @Input() destino:  DestinoViaje;
   @Input("idx") position: number;

  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) { 
    this.clicked = new EventEmitter();  
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  };

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino)) ;
    return false;
  }

}
