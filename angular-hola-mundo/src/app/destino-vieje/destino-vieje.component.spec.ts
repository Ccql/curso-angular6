import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoViejeComponent } from './destino-vieje.component';

describe('DestinoViejeComponent', () => {
  let component: DestinoViejeComponent;
  let fixture: ComponentFixture<DestinoViejeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoViejeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoViejeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
